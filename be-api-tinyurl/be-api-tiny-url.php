<?php
/**
 * Plugin Name:     Be API - TinyURL
 * Plugin URI:      www.beapi.fr
 * Description:     Generates TinyURL encoded permalinks in post edit
 * Author:          Jeremy Kervran
 * Author URI:      www.jeremykervran.fr
 * Text Domain:     be-api-tiny-url
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Be_Api_Tiny_Url
 */

class Be_Api_Tiny_URL {
    public function __construct() {
        add_action( 'admin_init', array( $this, 'create_tinyurl_acf_field' ) );
        add_filter( 'acf/load_field/name=tinyurl', array( $this, 'populate_tinyurl_acf_field' ) );
    }

    /**
     * Create the ACF field that will receive the post TinyURL
     *
     * @return void
     */
    public function create_tinyurl_acf_field(): void {
        if ( function_exists( 'acf_add_local_field_group' ) ) {
            acf_add_local_field_group( array(
                'key' => 'group_5fc27f7408119',
                'title' => 'TinyURL',
                'fields' => array(
                    array(
                        'key' => 'field_5fc27f7ba4460',
                        'label' => 'TinyURL',
                        'name' => 'tinyurl',
                        'type' => 'url',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'post',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
            ));
            
        }
    }

    /**
     * Generates a TinyURL encoded url, through the TinyURL API
     *
     * @param $url
     *
     * @return string
     */
    public function generate_tinyurl( $url ): string {
        $ch      = curl_init();
        $timeout = 5;

        curl_setopt( $ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
        $data = curl_exec( $ch );
        curl_close( $ch );

        return $data;
    }

    /**
     * @param $field
     *
     * @return array
     */
    public function populate_tinyurl_acf_field( $field ): array {
        // If we are not in the admin section, we return the field without modification
        if ( ! is_admin() ) {
            return $field;
        }

        // Define the TinyURL encoded post permarlink as default value for the field
        $field['value'] = $this->generate_tinyurl( get_post_permalink() );

        return $field;
    }
}

new Be_Api_Tiny_URL();