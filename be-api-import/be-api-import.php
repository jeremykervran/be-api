<?php
	/**
	 * Plugin Name:     Be API - Import
	 * Plugin URI:      www.beapi.fr
	 * Description:     Create posts from a JSON file
	 * Author:          Jérémy Kervran
	 * Author URI:      www.jeremykervran.fr
	 * Text Domain:     be-api-import
	 * Domain Path:     /languages
	 * Version:         0.1.0
	 *
	 * @package         Be_Api_Import
	 */


	if ( defined( 'WP_CLI' ) && WP_CLI ) {

		class WP_CLI_BeAPI_Import extends WP_CLI_Command {

			/**
			 * Parse the data from the JSON file
			 *
			 * @return array
			 */
			public function parse_json_data(): array {
				// Récupérer les données du JSON
				$json_url = plugin_dir_path( __FILE__ ) . 'json/generated.json';
				$json_file = file_get_contents( $json_url );
				if ( ! $json_file ) {
					WP_CLI::error( "ERROR: Can't read file at $json_url" );
				} else {
					WP_CLI::line( "Reading $json_url");
				}

				$data = json_decode( $json_file );

				return $data;
			}

			/**
			 * Check for existing posts with the JSON obj ID before creating the post
			 *
			 * @param $id
			 * @return bool
			 */
			public function check_existing_post_id( $id ): bool {
				if ( ! is_integer( $id ) ) {
					return false;
				}
				
				if ( post_exists( $id ) ) {
					return true;
				}

				return false;
			}

			/**
			 * Handles the image upload from JSON obj URL & sets it as the given post ID thumbnail
			 *
			 * @param string $url
			 * @param int $post_id
			 * @param string $imgDesc
			 * @return object
			 */
			public function upload_and_set_featured_image_from_url ( string $url, int $post_id, string $imgDesc ): object {			
				$file_array = array();
				$file_array['name'] = basename( $url );
			
				// Download file to temp location.
				$file_array['tmp_name'] = download_url( $url );
			
				// If error storing temporarily, return the error.
				if ( is_wp_error( $file_array['tmp_name'] ) ) {
					return $file_array['tmp_name'];
				}
			
				// Do the validation and storage stuff.
				$id = media_handle_sideload( $file_array, $post_id, $imgDesc );
			
				// If error storing permanently, unlink.
				if ( is_wp_error( $id ) ) {
					@unlink( $file_array['tmp_name'] );
					return $id;
				}

				return set_post_thumbnail( $post_id, $id );
			}

			/**
			 * Inserts the posts from the JSON data
			 *
			 * @return void
			 */
			public function insert_json_posts(): void {
				// réupérer les data parse par parse_json_data
				// insérer les posts via wp_insert_post avec les data
				// vérifier que l'id n'existe pas, via check_existing_post_id

				$data = $this->parse_json_data();

				if ( ! $data) {
					WP_CLI::error("Can't retrieve JSON data");
				} else {
					foreach( $data as $obj ) {
						// Retrieve the JSON object ID
						$index = $obj->index;
	
						// Prepare the post arguments array with JSON obj data
						$post_args = array(
							'ID'           => (int)$obj->index,
							'guid'         => $obj->guid,
							'post_title'   => filter_var( $obj->name, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW ),
							'post_content' => utf8_encode( $obj->content ),
							'post_author'  => 1,
							'post_status'  => 'publish',
							'post_type'    => 'post',
						);
	
						// Check if the JSON obj ID is already associated with a WP Post, and insert accordingly
						if ( $this->check_existing_post_id( $obj->index ) ) {
							$insert_post = wp_update_post($post_args);

							WP_CLI::success( "Updated post with ID $obj->index" );
						} else {
							$insert_post = wp_insert_post($post_args);

							WP_CLI::success( "Created post '$obj->name' with ID $obj->index" );
						}
	
						// If the post was created successfully, we upload and set the featured image from the JSON obj picture
						if ( ! $insert_post ) {
							WP_CLI::error( "Can't insert post with index $obj->index" );
						} else {
							$imageUpload = $this->upload_and_set_featured_image_from_url($obj->picture, $insert_post, $obj->name);
							$imageUpload ? WP_CLI::success( "Added $obj->picture as featured image for post #$insert_post" ) : WP_CLI::error( "Couln't upload image $obj->picture" );
						}
	
					}

				}

			}

		}

		WP_CLI::add_command( 'beapi-import', 'WP_CLI_BeAPI_Import' );
	}

